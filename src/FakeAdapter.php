<?php

namespace Nino\FakeAbstractAdapter;

use Symfony\Component\Cache\Adapter\AbstractAdapter;

class FakeAdapter extends AbstractAdapter
{
    /**
     * @see AbstractAdapter::__construct()
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected function doFetch(array $ids)
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    protected function doHave(string $id): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    protected function doClear(string $namespace)
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    protected function doDelete(array $ids)
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    protected function doSave(array $values, int $lifetime)
    {
        return true;
    }
}
