# FakeAbstractAdapter

An AbstractAdapter saving absolutely nothing.

# Usage
Require it `composer require nino/fake-abstract-adapter`, and use it as you would use any Cache Adapters.

```php
use Nino\FakeAbstractAdapter\FakeAdapter;

$cache = new FakeAdapter();
```

Symfony documentation for cache : https://symfony.com/doc/current/components/cache.html